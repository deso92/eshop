@extends('admin_layout')
@section('admin_content')

<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>
        <a href="index.html">Home</a>
        <i class="icon-angle-right"></i> 
    </li>
    <li>
        <i class="icon-edit"></i>
        <a href="#">Ajout de nouveau produit</a>
    </li>
</ul>

<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Ajout de nouveau produit</h2>
           
        </div>
        <p class="alert-success">
         <?php
         $message=Session::get('message');
         if($message){
           echo $message;
           Session::put('message',null);
         }
         ?>
        </p>
        <div class="box-content">
            <form class="form-horizontal" action="{{ URL('/save-product')}}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
              <fieldset>
                <div class="control-group">
                  <label class="control-label" for="date01">Nom de produit</label>
                  <div class="controls">
                    <input type="text" class="input-xlarge" name="product_name" required>
                  </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="selectError3">Categorie de produit</label>
                    <div class="controls">
                      <select id="selectError3" name="category_id">
                          <option>Veuillez selection une categorie</option>
                  <?php
                          $all_published_category=DB::table('tbl_categorie')
                                    ->where('category_status',1)	
                                    ->get();
                          foreach($all_published_category as $v_all_published_category){?>
                      <option value="{{ $v_all_published_category->category_id }}">{{ $v_all_published_category->category_name }}</option>
                  <?php } ?>
                      </select>
                  
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="selectError3">La marque</label>
                    <div class="controls">
                      <select id="selectError3" name="manufacture_id">
                        <option>Veuillez selection une marque</option>
                  <?php
                          $all_published_manufacture=DB::table('tbl_manufacture')
                                    ->where('publication_status',1)	
                                    ->get();
                          foreach($all_published_manufacture as $v_all_published_manufacture){?>
                      <option value="{{ $v_all_published_manufacture->manufacture_id }}">{{$v_all_published_manufacture->manufacture_name}}</option>
                  <?php } ?>
                      </select>
                    </div>
                </div>
          
                <div class="control-group hidden-phone">
                  <label class="control-label" for="textarea2">petite description de produit</label>
                  <div class="controls">
                    <textarea class="cleditor" name="product_short_description" rows="3" required></textarea>
                  </div>
                </div>

                <div class="control-group hidden-phone">
                    <label class="control-label" for="textarea2">longue description de produit</label>
                    <div class="controls">
                      <textarea class="cleditor" name="product_long_description" rows="3" required></textarea>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="date01">Le prix de produit</label>
                    <div class="controls">
                      <input type="text" class="input-xlarge" name="product_price" required>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="fileInput">Image</label>
                    <div class="controls">
                    <input class="input-file uniform_on" name="product_image" id="fileInput" type="file">
                    </div>
                  </div>  
                  
                  <div class="control-group">
                      <label class="control-label" for="date01">La taille de produit</label>
                      <div class="controls">
                        <input type="text" class="input-xlarge" name="product_size" required>
                      </div>
                  </div>

                  <div class="control-group">
                      <label class="control-label" for="date01">La couleur de produit</label>
                      <div class="controls">
                        <input type="text" class="input-xlarge" name="product_color" required>
                      </div>
                  </div>

                <div class="control-group hidden-phone">
                    <label class="control-label" for="textarea2">Statut de publication</label>
                    <div class="controls">
                    <input type="checkbox" name="publication_status" value="1">
                    </div>
                  </div>
                <div class="form-actions">
                  <button type="submit" class="btn btn-primary">Ajouter le produit</button>
                  <button type="reset" class="btn">Annuler</button>
                </div>
              </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->

@endsection
