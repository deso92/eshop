@extends('admin_layout')
@section('admin_content')

<ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="index.html">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="#">Tables</a></li>
    </ul>

    <div class="row-fluid sortable">		
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon user"></i><span class="break"></span>Members</h2>
               
            </div>
            <p class="alert-success">
                    <?php
                    $message=Session::get('message');
                    if($message){
                      echo $message;
                      Session::put('message',null);
                    }
                    ?>
                   </p>
            <div class="box-content">
                <table class="table table-striped table-bordered bootstrap-datatable datatable">
                  <thead>
                      <tr>
                          <th>Numero de marque</th>
                          <th>Designation</th>
                          <th>Description</th>
                          <th>Status</th>
                          <th>Actions</th>
                      </tr>
                  </thead>  
                  @foreach($all_manufacture_info as $all_manufacture) 
                  <tbody>
                    <tr>
                        <td>{{$all_manufacture->manufacture_id }}</td>
                        <td class="center">{{$all_manufacture->manufacture_name }}</td>
                        <td class="center">{{$all_manufacture->manufacture_description }}</td>
                        <td class="center">
                            @if($all_manufacture->publication_status==1)
                            <span class="label label-success">Actif</span>
                            @else
                                <span class="label label-danger">Inactif</span>
                            @endif
                        </td>
                        <td class="center">
                            @if($all_manufacture->publication_status==1)
                            <a class="btn btn-danger" href="{{URL::to('/unactive_manufacture/'.$all_manufacture->manufacture_id)}}">
                                <i class="halflings-icon white thumbs-down"></i>  
                            </a>
                            @else
                            <a class="btn btn-success" href="{{URL::to('/active_manufacture/'.$all_manufacture->manufacture_id)}}">
                                    <i class="halflings-icon white thumbs-up"></i>  
                                </a>
                            @endif
                            <a class="btn btn-info" href="{{URL::to('/edit-manufacture/'.$all_manufacture->manufacture_id)}}">
                                <i class="halflings-icon white edit" ></i>  
                            </a>
                            <a class="btn btn-danger" href="{{URL::to('/delete-manufacture/'.$all_manufacture->manufacture_id)}}" onclick="return confirmDelete()">
                                <i class="halflings-icon white trash" ></i> 
                            </a>
                        </td>
                    </tr>
                  </tbody>
                  @endforeach
              </table>            
            </div>
        </div><!--/span-->
    
    </div><!--/row-->

@endsection
