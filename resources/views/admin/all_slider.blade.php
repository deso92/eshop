@extends('admin_layout')
@section('admin_content')

<ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="index.html">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="#">Tables</a></li>
    </ul>

    <div class="row-fluid sortable">		
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon user"></i><span class="break"></span>Les sliders</h2>
               
            </div>
            <p class="alert-success">
                    <?php
                    $message=Session::get('message');
                    if($message){
                      echo $message;
                      Session::put('message',null);
                    }
                    ?>
                   </p>
            <div class="box-content">
                <table class="table table-striped table-bordered bootstrap-datatable datatable">
                  <thead>
                      <tr>
                          <th>ID slider</th>
                          <th>Image</th>
                          <th>Status</th>
                          <th>Actions</th>
                      </tr>
                  </thead>  
                  <?php  
                  $all_slider_info=DB::table('tbl_slider')->get();
                  ?>
                  @foreach($all_slider_info as $_vall_slider_info) 
                  <tbody>
                    <tr>
                        <td>{{$_vall_slider_info->slider_id }}</td>
                        <td><img src="{{URL::to($_vall_slider_info->slider_image)}}" style="height: 100px; width: 200px;"> </td>
                        <td class="center">
                            @if($_vall_slider_info->publication_status==1)
                            <span class="label label-success">Actif</span>
                            @else
                                <span class="label label-danger">Inactif</span>
                            @endif
                        </td>
                        <td class="center">
                            @if($_vall_slider_info->publication_status==1)
                            <a class="btn btn-danger" href="{{URL::to('/unactive_slider/'.$_vall_slider_info->slider_id)}}">
                                <i class="halflings-icon white thumbs-down"></i>  
                            </a>
                            @else
                            <a class="btn btn-success" href="{{URL::to('/active_slider/'.$_vall_slider_info->slider_id)}}">
                                    <i class="halflings-icon white thumbs-up"></i>  
                                </a>
                            @endif
                            <a class="btn btn-info" href="{{URL::to('/edit-slider/'.$_vall_slider_info->slider_id)}}">
                                <i class="halflings-icon white edit" ></i>  
                            </a>
                            <a class="btn btn-danger" href="{{URL::to('/delete-slider/'.$_vall_slider_info->slider_id)}}" onclick="return confirmDelete()">
                                <i class="halflings-icon white trash" ></i> 
                            </a>
                        </td>
                    </tr>
                  </tbody>
                  @endforeach
              </table>            
            </div>
        </div><!--/span-->
    
    </div><!--/row-->

@endsection
