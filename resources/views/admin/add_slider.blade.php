@extends('admin_layout')
@section('admin_content')

<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>
        <a href="index.html">Home</a>
        <i class="icon-angle-right"></i> 
    </li>
    <li>
        <i class="icon-edit"></i>
        <a href="#">Ajout de nouveau slider</a>
    </li>
</ul>

<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>ajout de slider</h2>
           
        </div>
        <p class="alert-success">
         <?php
         $message=Session::get('message');
         if($message){
           echo $message;
           Session::put('message',null);
         }
         ?>
        </p>
        <div class="box-content">
            <form class="form-horizontal" action="{{ URL('/save-slider')}}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
              <fieldset>

                <div class="control-group">
                    <label class="control-label" for="fileInput">Image de slider</label>
                    <div class="controls">
                    <input class="input-file uniform_on" name="slider_image" id="fileInput" type="file">
                    </div>
                  </div>  
                 

                <div class="control-group hidden-phone">
                    <label class="control-label" for="textarea2">Statut de publication</label>
                    <div class="controls">
                    <input type="checkbox" name="publication_status" value="1">
                    </div>
                  </div>
                <div class="form-actions">
                  <button type="submit" class="btn btn-primary">Ajouter de slider</button>
                  <button type="reset" class="btn">Annuler</button>
                </div>
              </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->

@endsection
