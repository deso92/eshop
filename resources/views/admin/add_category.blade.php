@extends('admin_layout')
@section('admin_content')

<ul class="breadcrumb">
    <li>
        <i class="icon-home"></i>
        <a href="index.html">Home</a>
        <i class="icon-angle-right"></i> 
    </li>
    <li>
        <i class="icon-edit"></i>
        <a href="#">Ajout de nouvelle categorie</a>
    </li>
</ul>

<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Ajout de nouvelle categorie</h2>
           
        </div>
        <p class="alert-success">
         <?php
         $message=Session::get('message');
         if($message){
           echo $message;
           Session::put('message',null);
         }
         ?>
        </p>
        <div class="box-content">
            <form class="form-horizontal" action="{{ URL('/save-category')}}" method="POST">
                {{ csrf_field() }}
              <fieldset>
                <div class="control-group">
                  <label class="control-label" for="date01">Nom de la categorie</label>
                  <div class="controls">
                    <input type="text" class="input-xlarge" name="category_name" required>
                  </div>
                </div>
          
                <div class="control-group hidden-phone">
                  <label class="control-label" for="textarea2">Description de la categorie</label>
                  <div class="controls">
                    <textarea class="cleditor" name="category_description" rows="3" required></textarea>
                  </div>
                </div>
                <div class="control-group hidden-phone">
                    <label class="control-label" for="textarea2">Statut de publication</label>
                    <div class="controls">
                    <input type="checkbox" name="category_status" value="1">
                    </div>
                  </div>

                  <div style="text-align:center;display:none" id="loaderDiv">
                  <img src="{{URL::to('backend/img/Spinner.gif')}}" width="150" alt="" />
                  </div>


                <div class="form-actions">
                  <button type="submit" id="btnSubmit" class="btn btn-primary">Valider</button>
                  <button type="reset" class="btn">Annuler</button>
                </div>
              </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->





@endsection
