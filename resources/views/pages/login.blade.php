@extends('layout')
@section('content')
<section id="form"><!--form-->
		<div class="container">
			<div class="row">
				<div class="col-sm-4 col-sm-offset-1">
					<div class="login-form"><!--login form-->
						<h2>Veuillez vous connecter</h2>
						<form action="#" method="post">
							<input type="email" required="" placeholder="Email" name="customer_email" />
							<input type="password" placeholder="Mot de passe" name="password" />
							
							<button type="submit" class="btn btn-default">Login</button>
						</form>
					</div><!--/login form-->
				</div>
				<div class="col-sm-1">
					<h2 class="or">OU</h2>
				</div>
				<div class="col-sm-4">
					<div class="signup-form"><!--sign up form-->
						<h2>Creer un nouveau compte</h2>
                        <form action="{{url('/customer_registration')}}" method="post">
                        {{csrf_field()}}
							<input type="text" placeholder="Nom complet" name="customer_name"/>
							<input type="email" placeholder="Adresse mail" name="customer_email"/>
                            <input type="password" placeholder="Mot de passe" name="password"/>
                            <input type="text" placeholder="Numero de contact" name="mobile_numer"/>
							<button type="submit" class="btn btn-default">Valider</button>
						</form>
					</div><!--/sign up form-->
				</div>
			</div>
		</div>
	</section><!--/form-->
@endsection
