-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  mer. 28 oct. 2020 à 16:35
-- Version du serveur :  10.1.36-MariaDB
-- Version de PHP :  5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `eshop`
--

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2020_01_31_181809_create_tbl_admin_table', 1),
(2, '2020_01_31_210112_create_tbl_category_table', 2),
(3, '2020_01_31_213110_create_tbl_categorie_table', 3),
(4, '2020_02_01_020318_create_tbl_manufacture_table', 4),
(5, '2020_02_01_120105_create_tbl_products_table', 5),
(6, '2020_02_01_161240_create_tbl_slider_table', 6),
(7, '2020_05_12_110726_create_tbl_customer_table', 7),
(8, '2020_07_31_222412_create_posts_table', 8);

-- --------------------------------------------------------

--
-- Structure de la table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `author` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `posts`
--

INSERT INTO `posts` (`id`, `title`, `body`, `author`, `created_at`, `updated_at`) VALUES
(1, 'title1', 'body1', 'author1', '2020-07-31 23:23:38', '2020-07-31 23:23:38'),
(2, 'title2', 'body2', 'author2', '2020-07-31 23:25:19', '2020-07-31 23:25:19'),
(3, 'title3', 'body3', 'author3', '2020-07-31 23:25:34', '2020-07-31 23:25:34');

-- --------------------------------------------------------

--
-- Structure de la table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `admin_id` int(10) UNSIGNED NOT NULL,
  `admin_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `tbl_admin`
--

INSERT INTO `tbl_admin` (`admin_id`, `admin_email`, `admin_password`, `admin_name`, `admin_phone`, `created_at`, `updated_at`) VALUES
(1, 'admin@gmail.com', 'adminadmin', 'desire', '12345', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `tbl_categorie`
--

CREATE TABLE `tbl_categorie` (
  `category_id` int(10) UNSIGNED NOT NULL,
  `category_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `tbl_categorie`
--

INSERT INTO `tbl_categorie` (`category_id`, `category_name`, `category_description`, `category_status`, `created_at`, `updated_at`) VALUES
(1, 'Alimentaire modif', 'test modif normal<br>', 1, NULL, NULL),
(2, 'Liqueur', 'Wisky et beaucoup de vins', 1, NULL, NULL),
(4, 'Electronique', 'Les appareilles electroniques<br>', 1, NULL, NULL),
(5, 'Telephone', 'Les portables de bonne qualites<br>', 1, NULL, NULL),
(6, 'Chaussure', 'Les chaussures de marques sebago et autres games disponibles<br>', 1, NULL, NULL),
(7, 'Pharmaceutique', 'les bien de soins corporels<br>', 1, NULL, NULL),
(8, 'Mobilier', 'Les bien de choses dispo<br>', 1, NULL, NULL),
(9, 'Immeuble', 'Les maisons de luxes a vendre et achat<br>', 1, NULL, NULL),
(10, 'Patisserie', 'Patisserie moderne', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `tbl_customer`
--

CREATE TABLE `tbl_customer` (
  `customer_id` int(10) UNSIGNED NOT NULL,
  `customer_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile_numer` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `tbl_customer`
--

INSERT INTO `tbl_customer` (`customer_id`, `customer_name`, `customer_email`, `password`, `mobile_numer`, `created_at`, `updated_at`) VALUES
(1, 'errr', 'deddd@gmail.com', 'rtrfr', '90905455', NULL, NULL),
(2, 'errr', 'deddd@gmail.com', 'fddd', '90905455', NULL, NULL),
(3, 'gff', 'deddd@gmail.com', 'fffff', '5666666666', NULL, NULL),
(4, 'eddd', 'deddd@gmail.com', 'ddddd', 'ddddd', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `tbl_manufacture`
--

CREATE TABLE `tbl_manufacture` (
  `manufacture_id` int(10) UNSIGNED NOT NULL,
  `manufacture_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `manufacture_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `publication_status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `tbl_manufacture`
--

INSERT INTO `tbl_manufacture` (`manufacture_id`, `manufacture_name`, `manufacture_description`, `publication_status`, `created_at`, `updated_at`) VALUES
(1, 'SEBAGO', 'Une nouvelle marque de Sebago<br>', 1, NULL, NULL),
(2, 'Adidas', 'Une marque de qualite <br>', 1, NULL, NULL),
(3, 'Samsung mode', 'Une marque de portable de haute qualite mode test<br>', 1, NULL, NULL),
(4, 'Itel', 'Nouvelle marque sur le marche<br>', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `tbl_products`
--

CREATE TABLE `tbl_products` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `manufacture_id` int(11) NOT NULL,
  `product_short_description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_long_description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_price` double(8,2) NOT NULL,
  `product_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_size` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_color` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `publication_status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `tbl_products`
--

INSERT INTO `tbl_products` (`product_id`, `product_name`, `category_id`, `manufacture_id`, `product_short_description`, `product_long_description`, `product_price`, `product_image`, `product_size`, `product_color`, `publication_status`, `created_at`, `updated_at`) VALUES
(6, 'Valencia', 6, 2, 'Qualite a moindre cout', 'chaussure de qualite', 30000.00, 'image/lhUrlxq8iNfrJjSTjl14.jpg', '33', 'blanche', 1, NULL, NULL),
(7, 'pantallon', 1, 2, 'haute qualite', 'nouvelle gamme&nbsp;', 150000.00, 'image/jLKB7BoVCH93RO5LuMZe.jpg', '34', 'blanche', 1, NULL, NULL),
(8, 'frigo', 4, 3, 'Bonne qualite', 'tres bonne qualite', 400000.00, 'image/YMMIqpcB4vKUmg3iEIJa.jpg', '15', 'cendre', 1, NULL, NULL),
(9, 'frigo', 4, 3, 'de bonne qualite', 'de tres bonne qualite', 350000.00, 'image/H3ZQI7Nb5yjlnTwpEACH.jpg', '34', 'blanche', 1, NULL, NULL),
(10, 'frigidaire', 4, 3, 'qualite superieure', 'de tres bonne bonne qualite', 350000.00, 'image/BLs8Y5SuBXFCUOjLljhy.jpg', '33', 'blanche', 1, NULL, NULL),
(11, 'Telephone samsung', 5, 3, 'android', 'telephone de qualite superieure', 450000.00, 'image/rXvuOA6XByawVTaenKaM.jpg', '15', 'cendre', 1, NULL, NULL),
(12, 'test', 2, 2, 'testtttt', 'tttttttttttttt', 350000.00, 'image/mZm1qUm3OZE5k3T3w8Rp.jpg', '33', 'blanche', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `tbl_slider`
--

CREATE TABLE `tbl_slider` (
  `slider_id` int(10) UNSIGNED NOT NULL,
  `slider_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `publication_status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `tbl_slider`
--

INSERT INTO `tbl_slider` (`slider_id`, `slider_image`, `publication_status`, `created_at`, `updated_at`) VALUES
(1, 'image/WUwsZGHhNmwaoMvk2qzQ.jpg', NULL, NULL, NULL),
(2, 'image/VS0bgZxuZfkKZqdnUd0j.jpg', NULL, NULL, NULL),
(3, 'image/aU8SfeCCzx1BDGTCAOMQ.jpg', NULL, NULL, NULL),
(4, 'image/UEzZipY6uvUWx4r4Qclj.jpg', NULL, NULL, NULL);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Index pour la table `tbl_categorie`
--
ALTER TABLE `tbl_categorie`
  ADD PRIMARY KEY (`category_id`);

--
-- Index pour la table `tbl_customer`
--
ALTER TABLE `tbl_customer`
  ADD PRIMARY KEY (`customer_id`);

--
-- Index pour la table `tbl_manufacture`
--
ALTER TABLE `tbl_manufacture`
  ADD PRIMARY KEY (`manufacture_id`);

--
-- Index pour la table `tbl_products`
--
ALTER TABLE `tbl_products`
  ADD PRIMARY KEY (`product_id`);

--
-- Index pour la table `tbl_slider`
--
ALTER TABLE `tbl_slider`
  ADD PRIMARY KEY (`slider_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `admin_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `tbl_categorie`
--
ALTER TABLE `tbl_categorie`
  MODIFY `category_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `tbl_customer`
--
ALTER TABLE `tbl_customer`
  MODIFY `customer_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `tbl_manufacture`
--
ALTER TABLE `tbl_manufacture`
  MODIFY `manufacture_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `tbl_products`
--
ALTER TABLE `tbl_products`
  MODIFY `product_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT pour la table `tbl_slider`
--
ALTER TABLE `tbl_slider`
  MODIFY `slider_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
