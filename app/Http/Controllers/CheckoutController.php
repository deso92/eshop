<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;

class CheckoutController extends Controller
{
    public function login_check(){
        return view('pages.login');
    }

    public function customer_registration(Request $request){
        $data=array();
        $data['customer_name'] = $request->customer_name;
        $data['customer_email'] = $request->customer_email;
        $data['password'] = $request->password;
        $data['mobile_numer'] = $request->mobile_numer;
         $customer_id=DB::table('tbl_customer')
                       ->insertGetId($data);

                       Session::put('customer_id',$customer_id);
                       Session::put('customer_name',$request->customer_name);
                       return Redirect('checkout');
    }

    public function checkout(){
        return view('pages.checkout');
    }
}
