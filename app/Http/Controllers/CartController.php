<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Cart;
use Illuminate\support\Facades\Redirect;

class CartController extends Controller
{
    public function add_to_cart(Request $request){
     //dd($request->all());
     $qty=$request->qty;
     $product_id=$request->product_id;
     $product_info=DB::table('tbl_products')
                      ->where('product_id',$product_id)
                      ->first();
                    Cart::add([
                      'id' => $product_id,
                      'qty'=>$qty,
                      'name'=>$product_info->product_name,
                      'price'=>$product_info->product_price,
                      'image'=>$product_info->product_image
                    ]);
                    return redirect::to('/cart-show');
        // $qty=$request->qty;
        // $product_id=$request->product_id;
        // $product_info=DB::table('tbl_products')
        //               ->where('product_id',$product_id)
        //               ->first();
        // $data['qty']=$qty;
        // $data['id']=$product_info->product_id;
        // $data['name']=$product_info->product_name;
        // $data['price']=$product_info->product_price;
        // $data['options']=$product_info->product_image;

        // Cart::add($data);
        // return redirect::to('/show-cart');
    }

    public function cartShow(){
        $cartprod=Cart::content();
        return view('pages.showCart')->with('cartprod',$cartprod);
        // $all_published_category=DB::table('tbl_categorie')
        //                        ->where('category_status',1)
        //                        ->get();

        // $manage_published_category=view('pages.add_to_cart')
        //         ->with('all_published_category',$all_published_category);
        // return view('layout')
        //         ->with('pages.add_to_cart',$manage_published_category);
    }

    public function delete_to_cart($rowId){
      Cart::update($rowId,0);
      return redirect::to('/cart-show');
    }

    public function update_cart(Request $request){
     $qty=$request->qty;
     $rowId=$request->rowId;

     Cart::update($rowId,$qty);
     return redirect::to('/cart-show');
    }
}
